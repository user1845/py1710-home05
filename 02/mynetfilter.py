# # Часть 2
#
# Напишите скрипт mynetfilter, которая выводит информацию о сетевых подключениях(используйте команда mynetstat)
#
# Программа поддерживаем следющие аргументы командной строки:
# --help - справка по программе
# --state - состояние соденинения, подключения в котором следует выводить, ESTABLISHED, если не задано
# --output - выходной файл, stdout если не задано
#
# Не пользуясь параметрами netstat, grep, etc. Парсим питоном.
# Нас интересует только секция
# Active Internet connections
# остальные секции стледует пропустить.


import sys
import argparse
import subprocess

SCRIPT = "mynetstat.py"
# NOTE:
# SCRIPT = "mynetstat.py" -> for real request netstat command - via script "mynetstat.py"
# SCRIPT = "fake_mynetstat.py" -> for "fake" request - via script "fake_mynetstat.py"
# that returns previously created file netstat.txt

def main():
    # 1) add expected optional args
    parser = argparse.ArgumentParser()
    parser.add_argument("--state", default="ESTABLISHED",
                        help="show connections only with specified state: --state <state> (default = ESTABLISHED)")
    parser.add_argument("--output", help="specify file for output")
    args = parser.parse_args()

    # 2) create variable to store output value, stdout will be by default
    out = sys.stdout
    if args.output:
        fp = open(args.output, "w")  # if output arg is provided create file for output writing
        out = fp
    else:
        fp = None

    # NOTE: exceptions and conditions with file name and format aren't done
    # user could enter any destination as a file for output
    # if file with provided name already exists, file will be overwritten

    # 3) run script (that is provided via constant SCRIPT) with default state and ouput
    res = subprocess.run(["python", SCRIPT], stdout=subprocess.PIPE, encoding="866")

    # NOTE: script mynetstat returns all strings that start with tcp/udp - as all active connections

    # 4) parse args:
    # state if not provided will be = ESTABLISHED
    # output if not provided = stdout

    for line in res.stdout.split("\n"): # convert strings to the lists
        line_list = line.split()
        if args.state in line_list:  # to show only connections with provided --state; if state isn't provided,default
            out.write(line + "\n") # write to the file out

    if fp:
        fp.close()  # close file if it was opened


if __name__=='__main__':
    main()


# TO DEBUG run like
# python mynetfilter.py - default, without state, without output file
# python mynetfilter.py --state ESTABLISHED ->  default state
# python mynetfilter.py --state TIME_WAIT -> not default state
# python mynetfilter.py --state EST -> not existing state, empty output
# python mynetfilter.py --state -> without state value => error: argument --state: expected one argument
# python mynetfilter.py --output out_file.txt -> with provided output file
# python mynetfilter.py --output out_file2 -> with provided output file without specified format
# python mynetfilter.py --output -> without provided output file => error: argument --output: expected one argument
# python mynetfilter.py --state TIME_WAIT --output out_file3.txt -> with state and file

