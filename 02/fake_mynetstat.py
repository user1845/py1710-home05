# # Домашнее задание:
#
# ## Часть 1
#
# Напишите скрипт mynetstat.
# Программа эмулирует работу netstat, всегда выводит одни и теже данные.
# Содержимое файл netstat.txt
#
#
# # Часть 2
#
# Напишите скрипт mynetfilter, которая выводит информацию о сетевых подключениях(используйте команда mynetstat)
#
# Программа поддерживаем следющие аргументы командной строки:
# --help - справка по программе
# --state - состояние соденинения, подключения в котором следует выводить, ESTABLISHED, если не задано
# --output - выходной файл, stdout если не задано
#
# Не пользуясь параметрами netstat, grep, etc. Парсим питоном.
# Нас интересует только секция
# Active Internet connections
# остальные секции следует пропустить.

# NOTE
# скрипт fake_mynetsta просто читает заданный файл

import os

filename = "netstat"
# file netstat should be in the same dir
# file should contain current values

# NOTE: cannot run file from current dir without using os pathgit commit

def main():

    with open(filename) as fp:
        for line in fp:
            #print(type(line))
            line_parts = line.split()
            # print(type(line_parts))
            #print(line_parts)
            proto = line_parts[0].lower()
            # print(proto)
            if proto.startswith("tcp") or proto.startswith("udp"):  # to show only active internet connections - that starts with tcp / udp
                 print(line)


if __name__ == '__main__':
   main()
