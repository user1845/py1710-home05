# # Домашнее задание:
#
# ## Часть 1
#
# Напишите скрипт mynetstat.
# Программа эмулирует работу netstat, всегда выводит одни и теже данные.

import subprocess

def main():
    # 1) run netstat command via subprocess
    res = subprocess.run("netstat -n", stdout=subprocess.PIPE, encoding="866")
    # NOTE: for fast check it will be better to change to the "netstat -n"

    # 2) parse output to get result with only active inet connections

    for line in res.stdout.split("\n"):
        line_parts = line.split() # convert strings to the lists
        if not line_parts:  # avoid extra spaces and strings that start with some descriptions
            continue
        proto = line_parts[0].lower()  # proto = first element in the lst if it is't space
        if proto in ("tcp", "udp",):  # to show only active internet connections - that starts with tcp / udp
            print(line)


if __name__ == '__main__':
   main()
