import sys
import subprocess
import re

import argparse

##pART1
def mynetstat(state, filepath):
     s = subprocess.run(["netstat"], stdout=subprocess.PIPE, encoding='866')

     if filepath is not None:
          with open(filepath, "a") as file:
               for line in s.stdout.split('\n'):
                    if line.find(state) != -1:
                         file.write(line + "\n")
          file.close()
     else:
          for line in s.stdout.split('\n'):
               if line.find(state)!= -1:
                    print(line)

##part2
parser = argparse.ArgumentParser()
parser.add_argument("--state", nargs='?', default="ESTABLISHED",
                    help="information about the connection state")
parser.add_argument("--output", nargs='?',
                    help="output file")
args = parser.parse_args()

mynetstat(args.state, args.output)


