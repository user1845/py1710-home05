import argparse
import subprocess
import sys

parser = argparse.ArgumentParser()
parser.add_argument("--state", nargs='?', default="ESTABLISHED",
                    help="information about the connection state")
parser.add_argument("--output", nargs='?',
                    help="output file")
args = parser.parse_args()



def mynetstat(state, filepath):
    s = subprocess.run([sys.executable, "mynetstat.py"], stdout=subprocess.PIPE, encoding='866')

    if filepath is not None:
        with open(filepath, "a") as file:
            for line in s.stdout.split('\n'):
                if line.find(state) != -1:
                    file.write(line + "\n")
        file.close()
    else:
        for line in s.stdout.split('\n'):
            if line.find(state) != -1:
                print(line)


mynetstat(args.state, args.output)
