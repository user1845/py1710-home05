import sys
import subprocess
import argparse


DEFAULT_STATE = 'ESTABLISHED'
HELP = '--help'
STATE = '--state'
OUTPUT = '--output'
MYNETSTAT = 'mynetstat.py'


parser = argparse.ArgumentParser(add_help=False)
parser.add_argument(HELP, nargs='?', help='Instructions')
parser.add_argument(STATE, nargs='?', default=DEFAULT_STATE)
parser.add_argument(OUTPUT, nargs='?',
                    help="information about the directory (the current directory by default)")
args = parser.parse_args()


def usage():
    print("""
Программа поддерживаем следющие аргументы командной строки:
--help - справка по программе
--state - состояние соденинения, подключения в котором следует выводить, ESTABLISHED, если не задано
--output - выходной файл, stdout если не задано
""")


def output(a):
    for line in a.stdout.split('\n'):
        print(line, end='\n')
    sys.exit()


def mynetfilter():

    res = subprocess.run(['python3.6', MYNETSTAT], stdout=subprocess.PIPE, encoding='utf-8')

    if len(sys.argv) < 2:
        output(res)

    param = sys.argv[1].lower()

    if param == HELP:
        usage()
    elif param == STATE:
        line_num = 1
        for line in res.stdout.split('\n'):
            if line_num in [1, 2]:
                line_num += 1
                print(line)
                continue
            elif line.startswith('Active'):
                break
            line_splited = line.split()
            if args.state is None:
                if DEFAULT_STATE in line_splited:
                    print(line)
            else:
                if args.state.upper() in line_splited:
                    print(line)

    elif param == OUTPUT:
        if args.output is None:
            output(res)
        else:
            with open('out.txt', 'w') as f:
                op = subprocess.run(['python3.6', MYNETSTAT], stdout=f, encoding='866')
                if op.returncode != 0:
                    print('command failed')
    else:
        sys.exit('Unknown command')


if __name__ == '__main__':
    mynetfilter()
