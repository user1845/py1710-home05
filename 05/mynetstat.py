#!/Users/dmitryilyuk/.pyenv/versions/3.6.2/bin/python
import pathlib

SOURCE_FILENAME = "netstat.txt"


def main():
    if not pathlib.Path(SOURCE_FILENAME).exists():
        print("File {} doesn't exist!".format(SOURCE_FILENAME))
        exit()

    with open(SOURCE_FILENAME, mode="r") as file:
        for line in file:
            print(line, end="")


if __name__ == '__main__':
    main()
