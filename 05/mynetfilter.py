#!/Users/dmitryilyuk/.pyenv/versions/3.6.2/bin/python
import argparse
import subprocess

LENGTH_CONNECTION_WITH_STATE = 6
LENGTH_IF_IS_SECTION_AND_COLUMNS = 2
STATE_INDEX = 5
SCRIPT_NAME = "mynetstat.py"
SECTION_NAME = "Active Internet connections"


def show_connections_by_state(state, file_to_save=None):
    res = subprocess.run(["python", "{}".format(SCRIPT_NAME)], stdout=subprocess.PIPE, encoding='utf-8')

    filtered_connections = []
    stdout_list = res.stdout.splitlines()

    for idx, line in enumerate(stdout_list):
        # First add section name + column names
        if line.startswith(SECTION_NAME):
            filtered_connections.append(line)
            filtered_connections.append(stdout_list[idx + 1])

        # If we already have necessary section add connections
        if len(stdout_list) > LENGTH_IF_IS_SECTION_AND_COLUMNS and (line.startswith("tcp") or line.startswith("udp")):
            if len(line.split()) == LENGTH_CONNECTION_WITH_STATE and line.split()[STATE_INDEX] == state:
                filtered_connections.append(line)
            # Break if necessary section is over
            if not stdout_list[idx + 1].startswith("tcp") and not stdout_list[idx + 1].startswith("udp"):
                break

    # Let's save or print
    if file_to_save:
        with open("{}".format(file_to_save), mode="w") as file:
            for line in filtered_connections:
                file.write(line + "\n")
    else:
        for line in filtered_connections:
            print(line)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--state", help="filter by state, default is ESTABLISHED", default="ESTABLISHED")
    parser.add_argument("--output", help="save output to file, default is stdout")
    args = parser.parse_args()

    show_connections_by_state(args.state, file_to_save=args.output)


if __name__ == '__main__':
    main()
