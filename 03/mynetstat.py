#!/usr/bin/python3.6


def netstat():
    with open("netstat.txt") as file:
        for line in file:
            if line.strip() == 'Proto Recv-Q Send-Q  Local Address          Foreign Address        (state)':
                break
        for line in file:
            if 'udp4' in line:
                break
            print(line)
        print(file.read())


if __name__ == '__main__':
    netstat()
