#!/usr/bin/python3.6

import subprocess
import argparse

parser = argparse.ArgumentParser(description='Mynetstat Parser')
parser.add_argument('--state', default='ESTABLISHED', help="ESTABLISHED if state not specified")
parser.add_argument('--output', help="output to console if file not specified")
args = parser.parse_args()


res = subprocess.run('./mynetstat.py', shell=True, stdout=subprocess.PIPE, universal_newlines=True)
for line in res.stdout.split('\n'):
    if args.state in line:
        if args.output is None:
            print(line)
        else:
            with open(args.output, "a") as f:
                f.write(line)
                f.write('\n')
