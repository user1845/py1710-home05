#!/usr/bin/python3
import argparse
import subprocess

IGNORED_SECTION = "Active Multipath Internet connections"


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--state", help="Filter netstat by state. ESTABLISHED by default", default="ESTABLISHED")
    parser.add_argument("--output", help="Write output into file or stdout by default", default="stdout")
    args = parser.parse_args()
    netstat = str(subprocess.run(["./mynetstat.py"], stdout=subprocess.PIPE).stdout)
    for line in netstat.split('\\n'):
        if IGNORED_SECTION in line:
            break
        if line[-len(args.state):] == args.state.upper():
            if args.output == 'stdout':
                print(line)
            else:
                with open(args.output, "a") as f:
                    f.writelines([line, "\n"])


if __name__ == '__main__':
    main()
